-- Inserting / creating records/rows
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Psy 6", "2012-01-01", 2
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Trip", "1996-01-01", 1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Gangnam Style", 253, "Kpop", 1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kundiman", 234, "OPM", 2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kisapmata", 259, "OPM", 2
);

-- Selecting / Retrieving records
-- display all columns for all songs

SELECT * FROM songs;

-- Display the title and genre of all songs

SELECT song_name, genre FROM songs;

-- Display title of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display title ang length of all OPM songs that are more than 2:40
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

-- Updating records
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- Removing the WHERE clause will update all rows

-- Deleting Records
DELETE FROM songs WHERE genre = "OPM" AND length > 240;
-- Removing the WHERE clause will delete all rows